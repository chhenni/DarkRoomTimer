#!/usr/bin/env python
import telnetlib
import time


IP_ADDRESS = "192.168.1.50"
PORT = 23


PING_INTERVAL = 60 * 5



class TPS2Interface():
    def __init__(self):
        self.lastMessage = 0
        print "TPS2: Connecting to TPS-2"
        self.socket =  telnetlib.Telnet(IP_ADDRESS, PORT)
        print "TPS2: Hopefully connected"
        

    def lightOn(self):
        print "TPS2: Turning lights on"
        self.__writeLine("/On 2")

    def lightOff(self):
        print "TPS2: Turning lights off"
        self.__writeLine("/Off 2")

    def tick(self):
        if time.time() > self.lastMessage + PING_INTERVAL:
            self.__writeLine("/H")

        self.socket.read_very_lazy()


    def quit(self):
        print "TPS2: Disconnecting"
        self.__writeLine("/X")
        self.socket.close()

    def __writeLine(self, line):
        print "TPS2: Sending {0}".format(line)
        self.socket.write(line + "\r\n")
        self.lastMessage = time.time()
