#!/usr/bin/env python
import time
PING_INTERVAL = 60 * 5

class DummyInterface():
    def __init__(self):
        self.lastMessage = time.time() + 5

    def lightOn(self):
        print "Dummy: Turning lights on"
        self.__writeLine("/On 2")

    def lightOff(self):
        print "Dummy: Turning lights off"
        self.__writeLine("/Off 2")

    def tick(self):
        if time.time() > self.lastMessage + PING_INTERVAL:
            self.__writeLine("/H")


    def quit(self):
        print "Dummy: Disconnecting"
        self.__writeLine("/X")

    def __writeLine(self, line):
        print "Dummy: Sending {0}".format(line)
        self.lastMessage = time.time()


