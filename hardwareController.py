
from multiprocessing import Process, Pipe, Queue
from hardware.dummy import DummyInterface
from hardware.tps2 import TPS2Interface

import time

TICK_INTERVAL = 1

class HardwareDispatcher():
    def __init__(self, messageQueue):
        self.interface = TPS2Interface()
        self.queue = messageQueue
        self.stop = False
        self.nextTick = 0

    def run(self):
        while not self.stop:
            while not self.queue.empty():
                try:
                    message = self.queue.get()
                    print message
                    command = message[0]

                    if command == "quit":
                        self.stop = True
                        self.interface.quit()
                    elif command == "on":
                        self.interface.lightOn()
                    elif command == "off":
                        self.interface.lightOff()
                except Exception, ex:
                    print ex

            if time.time() > self.nextTick:
                self.nextTick = time.time() + TICK_INTERVAL
                self.interface.tick()

            time.sleep(0.001)


def start(messageQueue):
    dispatcher = HardwareDispatcher(messageQueue)
    dispatcher.run()

def turnOn(messageQueue):
    messageQueue.put(["on"])

def turnOff(messageQueue):
    messageQueue.put(["off"])

def quit(messageQueue):
    messageQueue.put(["quit"])