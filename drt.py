#!/usr/bin/env python

# http://pygame.org/hifi.html
# https://bitbucket.org/chhenni/raspbot_remote/src/34a3b8d2f0410167e41cd2e4b8279a837c3a02d0/remote.py?fileviewer=file-view-default

import pygame
from multiprocessing import Process, Queue, freeze_support
from threading import Thread
from pygame.locals import *
from hardware.dummy import *

import display
import hardwareController
import time


USE_PROC = True

hardwareQueue = Queue()

clock = pygame.time.Clock()

hardwareDispatcherProcess = None

def lightOn():
    hardwareController.turnOn(hardwareQueue)

def lightOff():
    hardwareController.turnOff(hardwareQueue)



if __name__ == '__main__':
    hwArgs = (hardwareQueue,)
    if USE_PROC:
        hardwareDispatcherProcess = Process(target=hardwareController.start, args=hwArgs)
    else:
        hardwareDispatcherProcess = Thread(target=hardwareController.start, args=hwArgs)

    hardwareDispatcherProcess.start()


    disp = display.Display()
    disp.lightOnCallback = lightOn
    disp.lightOffCallback = lightOff

    print "Running"
    while not disp.quitting:
        clock.tick(100)
        disp.tick()

        #clock.tick(100)

    hardwareController.quit(hardwareQueue)
    if USE_PROC:
        hardwareDispatcherProcess.terminate()
